#!/usr/bin/env python3
from itertools import zip_longest
from pathlib import Path
import sys

if __name__ == '__main__':
    base = Path('/etc/nginx/sites-enabled')
    domains = []

    if not base.is_dir():
        sys.exit("{} is not a directory".format(base.as_posix()))

    for filename in base.iterdir():
        with open(filename.resolve().as_posix(), 'r') as f:
            for line in f.readlines():
                line = line.strip()
                if line.startswith('server_name'):
                    domains += line.split()[1:]
    for domain in domains:
        print('-d', domain.strip(';'), end=' ')
