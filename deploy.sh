#!/bin/sh
ANSIBLE_STDOUT_CALLBACK=debug ansible-playbook -i inventory --vault-password-file=password.txt setup.yml --extra-vars '@secrets.yml'
